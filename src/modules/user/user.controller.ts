/* eslint-disable @typescript-eslint/no-unused-vars */
import { Controller, Get, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: '接口 user get test' })
  @Get('test-get')
  getUserInfo(): any {
    return this.userService.getUserInfo();
  }
  @ApiOperation({ summary: '接口 user post test' })
  @Post('test-post')
  getUserInfo1(): any {
    return this.userService.getUserInfo();
  }
}
