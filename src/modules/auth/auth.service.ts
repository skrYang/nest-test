import { Injectable, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthInfoDto } from './dto/auth-info.dto';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  createToken(data: any) {
    return this.jwtService.sign({ ...data });
  }

  // 登录
  async login(ctx: AuthInfoDto) {
    const { username, password } = ctx;
    const data: any = {
      id: 'fdsfqwqweqweqe',
      username,
    };
    const token = this.createToken({
      id: data.id,
      username: data.username,
      password,
    });
    return Object.assign(
      { data: token },
      { msg: HttpStatus['200'], code: HttpStatus.OK },
    );
  }
}
