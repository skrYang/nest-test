import { Controller, Post, Body, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthPipe } from './dto/auth.pipe';
import { AuthInfoDto } from './dto/auth-info.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: '登录' })
  @HttpCode(HttpStatus.OK)
  @Post('login')
  async login(@Body(AuthPipe) ctx: AuthInfoDto) {
    return await this.authService.login(ctx);
  }
}
