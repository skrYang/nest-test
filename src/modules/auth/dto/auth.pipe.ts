/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

@Injectable()
export class AuthPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    // 手动单独校验 已修改为全局自动校验
    // const DTO = plainToInstance(metadata.metatype, value);
    // const errors = await validate(DTO);
    // console.log(errors);
    // if (errors.length > 0) {
    //   throw new HttpException('error', HttpStatus.BAD_REQUEST);
    // }
    return value;
  }
}
