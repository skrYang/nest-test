import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class AuthInfoDto {
  @ApiProperty({ description: '用户名', example: 'username' })
  @IsString({ message: '用户名为字符串类型' })
  @IsNotEmpty({ message: '用户名不能为空' })
  @MinLength(5, { message: '账号至少5个字符' })
  @MaxLength(20, { message: '账号最多20个字符' })
  readonly username: string;

  @ApiProperty({ description: '密码', example: 'password' })
  @IsString({ message: 'password 类型错误，正确类型 string' })
  @IsNotEmpty({ message: 'password 不能为空' })
  password: string;
}
