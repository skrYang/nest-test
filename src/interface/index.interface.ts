// 分页
export interface Pagination {
  page?: number;
  limit?: number;
}

export interface UserQueryOptions extends Pagination {
  username?: string;
}
