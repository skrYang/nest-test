import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './modules/app/app.module';
import { generateDocument } from './api-doc';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // 设置全局前缀
  app.setGlobalPrefix('api');
  // 全局注册错误的过滤器
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  // 创建文档
  generateDocument(app);
  // 端口
  await app.listen(7979);
}
bootstrap();
